﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace examen_final
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables
            int opcion = 0;
            string contra = "";
            string nombreUsuario = "";
            string apellidoUsuario = "";
            string contraUsuario = "";
            string correoUsuario = "";
            string CONNETION_STRING = "Data Source=LPT82N1S72;Database=ExamenFinal;User ID=miguel;Password=Abcd@1234";
            SqlConnection conexion = new SqlConnection(CONNETION_STRING);
            string query = "";
            int id = 0;
            SqlCommand comando = null;
            SqlDataReader lector = null;
            int idSeleccionado = 0;
            string iniciarCorreo = "";
            string iniciarContra = "";
            string correoDB = "";
            string contraDB = "";
            int idDB = 0;
            int opcion2 = 0;
            int idParaEnviar = 0;
            string asuntoCorreo = "";
            string mensajeCorreo = "";
            int leerCorreo = 0;
            //Bucle principal del programa que correra mientras la opcion no sea 4
            do
            {
                opcion = MenuPrincipal(); //Funcion que imprime el menu, y guarda la opcion elegida.

                //Metodo Switch para validar la opcion seleccionada
                switch (opcion)
                {
                    //Opcion #1 - Crear usuario
                    case 1:
                        
                        Console.Clear();
                        Console.Write("Por favor indique la contraseña del usuario administrador: ");
                        contra = Console.ReadLine();
                        Console.Clear();

                        //Validar la contraseña dentro del sistema para poder crear un usuario.
                        if (contra == "Abcd1234")
                        {
                            //Inicio de la solicitu de datos del usuario
                            Console.WriteLine("A continuacion se van a solicitar los datos del usuario.");
                            
                            Console.Write("\nIndique el primer nombre del usuario: ");
                            nombreUsuario = Console.ReadLine().ToLower();
                            
                            Console.Write("\nIndique el primer apellido del usuario: ");
                            apellidoUsuario = Console.ReadLine().ToLower();

                            Console.Write("\nIndique la contraseña del usuario: ");
                            contraUsuario = Console.ReadLine().ToLower();

                            correoUsuario = $"{nombreUsuario}.{apellidoUsuario}@grupo7.com"; //interpilar para que el correo sea el nombre y el apellido del usuario agregando grupo7 como dominio de correo.

                            Console.WriteLine($"\nEl correo del usuario es: {correoUsuario}");
                            Console.ReadKey();
                            //fin de la solicitud de datos del usaurio

                            //Inicio del metodo para ingresar los datos del usurio a la base de datos
                            IngresarUsuario(query, nombreUsuario, apellidoUsuario, correoUsuario, contraUsuario, comando, conexion, id);
                            
                        }
                        else //Caso en que se ingrese una contraseña incorrecta.
                        {
                            Console.WriteLine("Contraseña Incorrecta.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;

                    case 2:
                        //Opcion 2 para cuando sea requerido modificar algun usuario en espeficio
                        Console.Clear();
                        Console.Write("Por favor indique la contraseña del usuario administrador: ");
                        contra = Console.ReadLine();
                        Console.Clear();

                        //Validar la contraseña dentro del sistema para poder crear un usuario.
                        if (contra == "Abcd1234")
                        {
                            UsariosDisponibles(query, comando, conexion, lector); //Metodo para imprimir los usuarios disponibles.

                            Console.Write("\nIndique el ID del usuario que el desea modificar: ");

                            try
                            {
                                idSeleccionado = Convert.ToInt32(Console.ReadLine());
                            }
                            catch (Exception e)
                            {
                                Console.Clear();
                                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            }

                            Console.Clear();
                            //Inicio de la solicitu de datos del usuario
                            Console.WriteLine("A continuacion se van a solicitar los datos del usuario.");

                            Console.Write("\nIndique el primer nombre del usuario: ");
                            nombreUsuario = Console.ReadLine().ToLower();

                            Console.Write("\nIndique el primer apellido del usuario: ");
                            apellidoUsuario = Console.ReadLine().ToLower();

                            Console.Write("\nIndique la contraseña del usuario: ");
                            contraUsuario = Console.ReadLine().ToLower();

                            correoUsuario = $"{nombreUsuario}.{apellidoUsuario}@grupo7.com"; //interpolar para que el correo sea el nombre y el apellido del usuario agregando grupo7 como dominio de correo.

                            Console.WriteLine($"\nEl correo del usuario es: {correoUsuario}");
                            Console.ReadKey();
                            //fin de la solicitud de datos del usaurio

                            //Inicio del metodo para modificar los datos del usurio a la base de datos
                            ModificarUsario(query, nombreUsuario, apellidoUsuario, correoUsuario, contraUsuario, idSeleccionado, comando, conexion);

                        }
                        else //Caso en que se ingrese una contraseña incorrecta.
                        {
                            Console.WriteLine("Contraseña Incorrecta.");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;

                    case 3:
                        Console.Clear();
                        
                        //Solicitar el correo y la contraseña para validar que sean exactos.
                        Console.WriteLine("Para ingresar al sistema debe primero iniciar sesion");
                        Console.Write("Correo: ");
                        iniciarCorreo = Console.ReadLine();

                        Console.Write("\nContraseña: ");
                        iniciarContra = Console.ReadLine();

                        try
                        {
                            //Query para validar que el usuario ingresado sea correcto.
                            query = $"SELECT id, correo, contraseña FROM usuario WHERE correo = '{iniciarCorreo}' AND contraseña = '{iniciarContra}';";
                            comando = new SqlCommand(query, conexion);

                            conexion.Open();

                            lector = comando.ExecuteReader();

                            while (lector.Read())
                            {
                                idDB = Convert.ToInt32(lector["id"]);
                                correoDB = lector["correo"].ToString();
                                contraDB = lector["contraseña"].ToString();
                            }
                            
                            lector.Close();
                            conexion.Close();

                            //Iniciar sesion con el usuario indicado
                            if (iniciarCorreo == correoDB && iniciarContra == contraDB)
                            {
                                Console.Clear();

                                do
                                {
                                    opcion2 = MenuSecundario(correoDB); //Funcion para imprimir el menu secundario

                                    //switch interno de la opcion 3
                                    switch (opcion2)
                                    {
                                        //opcion 1 enviar un correo.
                                        case 1:
                                            Console.Clear();
                                            UsariosDisponibles(query, comando, conexion, lector); //Metodo para imprimir los usuarios disponibles.

                                            Console.Write("Indique el ID del correo que desea: ");

                                            try
                                            {
                                                idParaEnviar = Convert.ToInt32(Console.ReadLine());
                                            }
                                            catch (Exception e)
                                            {
                                                Console.Clear();
                                                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                                                Console.ReadKey();
                                                Console.Clear();
                                                break;
                                            }

                                            //Escribir el asunto
                                            Console.Write("Asunto: ");
                                            asuntoCorreo = Console.ReadLine();

                                            //Escribir el Mensaje 
                                            Console.WriteLine("Mensaje:");
                                            mensajeCorreo = Console.ReadLine();

                                            EnviarCorreo(query, idDB, idParaEnviar, asuntoCorreo, mensajeCorreo, comando, conexion);

                                            break;

                                        //Opcion dos para mostrar la lista de correos recibidos.
                                        case 2:
                                            MostrarListaCorreos(query, idDB, comando, conexion, lector);

                                            Console.Write("\nIndique el ID del correo que desea leer o precione 0 para salir: ");

                                            try
                                            {
                                                leerCorreo = Convert.ToInt32(Console.ReadLine());
                                            }
                                            catch (Exception e)
                                            {
                                                Console.Clear();
                                                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                                                Console.ReadKey();
                                                Console.Clear();
                                                break;
                                            }

                                            Console.Clear();

                                            if(leerCorreo != 0)
                                            {
                                                //Mostrar el correo seleccionado
                                                MostrarCorreoSeleccionado(query, leerCorreo, comando, conexion, lector);
                                                Console.Write("Precione cualquier tecla para salir");
                                                Console.ReadKey();
                                                Console.Clear();
                                                
                                            } else
                                            {
                                                Console.WriteLine("Se regresado al menu del correo");
                                                Console.ReadKey();
                                                Console.Clear();
                                            }

                                            break;

                                        //Cuando se selecciona la opcion 3 que es cerrar sesion.
                                        case 3:
                                            Console.Clear();
                                            Console.WriteLine("Se cerrara la sesion de su correo");
                                            Console.ReadKey();
                                            Console.Clear();
                                            break;
                                    }
                                } while (opcion2 != 3);

                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine("Correo o contraseña son incorrectos.");
                                Console.ReadKey();
                            }

                        }
                        catch (Exception e)
                        {
                            Console.Clear();
                            Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                            Console.ReadKey();
                            Console.Clear();

                        }
                        finally
                        {
                            lector.Close();
                            conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
                        }

                        break;
                }


            } while (opcion != 4);
            Console.Clear();
            Console.WriteLine("Tenga un buen dia.");
        }

        //Funcion para imprimir el Menu Principal y caputrar la opcion del usuario.
        static int MenuPrincipal()
        {
            int opcion = 0; //variable opcion para capturar eleccion

            //Impresion de la lista de Menu
            Console.WriteLine("Bienvenidos al sistema de correos del Grupo 7.\n");
            Console.WriteLine("1 - Crear un usuario. (Requiere contraseña de admin.)");
            Console.WriteLine("2 - Modificar un usuario. (Requiere contraseña de admin.)");
            Console.WriteLine("3 - Iniciar Sesion.");
            Console.WriteLine("4 - Cerrar el programa. \n");
            Console.Write("Por favor indique el numero de la opcion que desea: ");

            //try catch
            try
            {
                opcion = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();
            }

            return opcion;
        }

        //Metodo para ingresar un usuario a la base de datos.
        static void IngresarUsuario(string query, string nombreUsuario, string apellidoUsuario, string correoUsuario, string contraUsuario, SqlCommand comando, SqlConnection conexion, int id)
        {
            try
            {
                //Query que ingresa los datos a la base de datos
                query = $"INSERT INTO dbo.usuario(nombre, apellido, correo, contraseña) OUTPUT INSERTED.id VALUES('{nombreUsuario}', '{apellidoUsuario}', '{correoUsuario}', '{contraUsuario}')";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                id = (int)comando.ExecuteScalar();
                Console.WriteLine("\nUsuario Registrado");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Metodo para mostrar los usuarios disponibles en la base de datos.
        static void UsariosDisponibles(string query, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = "SELECT id, correo FROM usuario";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();
                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"ID: {lector["id"].ToString()} | Correo: {lector["correo"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Metodo para modificar un usuario en especifico. 
        static void ModificarUsario(string query, string nombreUsuario, string apellidoUsuario, string correoUsuario, string contraUsuario,  int idSeleccionado,SqlCommand comando, SqlConnection conexion)
        {
            try
            {
                //Query que ingresa los datos a la base de datos
                query = $"update dbo.usuario set nombre = '{nombreUsuario}', apellido = '{apellidoUsuario}', correo = '{correoUsuario}', contraseña = '{contraUsuario}' where id = {idSeleccionado};";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                comando.ExecuteScalar();
                Console.WriteLine("\nUsuario Modificado");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Funcion que imprime el menu secundario
        static int MenuSecundario(string correoDB)
        {
            int opcion = 0; //variable opcion para capturar eleccion

            //Impresion de la lista de Menu
            Console.WriteLine($"Bienvenidos a su buzon de correo: {correoDB}\n");
            Console.WriteLine("1 - Enviar Correo");
            Console.WriteLine("2 - Mostrar Correos Recibidos");
            Console.WriteLine("3 - Salir");
            Console.Write("Por favor indique el numero de la opcion que desea: ");

            //try catch
            try
            {
                opcion = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();
            }

            return opcion;
        }

        //Metodo para enviar un correo
        static void EnviarCorreo(string query, int idDB, int idParaEnviar, string asuntoCorreo, string mensajeCorreo, SqlCommand comando, SqlConnection conexion)
        {
            try
            {
                Console.Clear();
                //Query que ingresa los datos a la base de datos
                query = $"INSERT INTO dbo.correo(remitenteid, destinoid, asunto, mensaje) VALUES({idDB}, {idParaEnviar}, '{asuntoCorreo}', '{mensajeCorreo}')";

                comando = new SqlCommand(query, conexion);
                conexion.Open();
                comando.ExecuteScalar();
                Console.WriteLine("\nCorreo enviado.");
                conexion.Close();
                Console.ReadKey();
                Console.Clear();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                conexion.Close(); //Cerrar la base de datos sin importar si se envio el correo o si hubo un error.
            }
        }

        //Metodo para mostrar la lista de correos de dicho usuario.
        static void MostrarListaCorreos(string query, int idDB, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = $"select c.id, correo as 'Remitente', asunto as 'Asunto' from usuario as u, correo as c where destinoid= {idDB} and u.id = remitenteid;";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();

                Console.Clear();

                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"ID: {lector["id"].ToString()} | Asunto: {lector["Asunto"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }

        //Mostrar el correo seleccionado.
        static void MostrarCorreoSeleccionado(string query, int leerCorreo, SqlCommand comando, SqlConnection conexion, SqlDataReader lector)
        {
            try
            {
                query = $"select asunto as 'Asunto', mensaje as 'Mensaje' from correo as c where id = {leerCorreo};";
                comando = new SqlCommand(query, conexion);

                conexion.Open();

                lector = comando.ExecuteReader();

                Console.Clear();

                Console.WriteLine("===============================");

                while (lector.Read())
                {
                    Console.WriteLine($"Asunto: {lector["Asunto"].ToString()} \nMensaje: {lector["Mensaje"].ToString()}\n");

                }

                lector.Close();

            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine($"Se produjo un error de tipo: {e.GetType().ToString()}");
                Console.ReadKey();
                Console.Clear();

            }
            finally
            {
                lector.Close();
                conexion.Close(); //Cerrar la base de datos sin importar si se registro el usaurio o si hubo un error.
            }
        }
    }
}
